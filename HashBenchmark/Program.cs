﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Diagnostics;
using KellermanSoftware.NetEncryptionLibrary;
using Org.BouncyCastle;
using org.jivesoftware.util;

namespace HashBenchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {


                Console.WriteLine("Insert size of the byte array to be hashed in bytes:");
                var size = int.Parse(Console.ReadLine());
                var randomBytearray = generateString(size);
                var key = generateString(16);
                var keyString = Convert.ToBase64String(key);
                TripleDesBenchmark(randomBytearray, key, keyString);
                Console.WriteLine("------------------------------------------------");
                AESBenchmark(randomBytearray, key, keyString);
                Console.WriteLine("------------------------------------------------");
                BlowfishBenchmark(randomBytearray, key, keyString);
                Console.WriteLine("------------------------------------------------");

            }
        }

        private static byte[] generateString(int v)
        {
            RandomNumberGenerator rng = new RNGCryptoServiceProvider();
            byte[] tokenData = new byte[v];
            rng.GetBytes(tokenData);


            return tokenData;
        }

        private static void TripleDesBenchmark(byte[] inblock, byte[] key, string keyString)
        {

            var Stopwatch = new Stopwatch();
            Stopwatch.Start();


            var tripleDESAlgorithm = TripleDESCryptoServiceProvider.Create();
            tripleDESAlgorithm.Mode = CipherMode.CBC;
            tripleDESAlgorithm.Padding = PaddingMode.PKCS7;
            tripleDESAlgorithm.KeySize = 128;

            tripleDESAlgorithm.GenerateIV();
            //tripleDESAlgorithm.GenerateKey();
            tripleDESAlgorithm.Key = key;

            var encryptor = tripleDESAlgorithm.CreateEncryptor();
            var decryptor = tripleDESAlgorithm.CreateDecryptor();

            var result = encryptor.TransformFinalBlock(inblock, 0, inblock.Length);

            Stopwatch.Stop();

            Console.WriteLine("TripleDes(Desede) - Crypt -  Encrypting cost in time in ms: {0}", Stopwatch.ElapsedMilliseconds);
            Console.WriteLine("Crypted file size: {0}", result.Length.ToString());

            Stopwatch.Reset();
            Stopwatch.Start();
            var decrypt = decryptor.TransformFinalBlock(result, 0, result.Length);
            Stopwatch.Stop();
            Console.WriteLine("TripleDes(Desede) - Decrypt - Encrypting cost in time in ms: {0}", Stopwatch.ElapsedMilliseconds);
            Console.WriteLine("Integryty check result: {0}", decrypt.SequenceEqual(inblock));


        }

        private static void AESBenchmark(byte[] inblock, byte[] key, string keyString)
        {
            var Stopwatch = new Stopwatch();
            Stopwatch.Start();

            var aesAlgorithm = SymmetricAlgorithm.Create();
            aesAlgorithm.Mode = CipherMode.CBC;
            aesAlgorithm.KeySize = 128;
            aesAlgorithm.Padding = PaddingMode.PKCS7;

            aesAlgorithm.GenerateIV();
            //aesAlgorithm.GenerateKey();
            aesAlgorithm.Key = key;

            var encryptor = aesAlgorithm.CreateEncryptor();
            var decryptor = aesAlgorithm.CreateDecryptor();

            var result = encryptor.TransformFinalBlock(inblock, 0, inblock.Length);

            Stopwatch.Stop();

            Console.WriteLine("AES - Crypt -  Encrypting cost in time in ms: {0}", Stopwatch.ElapsedMilliseconds);
            Console.WriteLine("Crypted file size: {0}", result.Length.ToString());

            Stopwatch.Reset();
            Stopwatch.Start();

            var decrypt = decryptor.TransformFinalBlock(result, 0, result.Length);
            Stopwatch.Stop();
            Console.WriteLine("AES - Decrypt - Encrypting cost in time in ms: {0}", Stopwatch.ElapsedMilliseconds);
            Console.WriteLine("Integryty check result: {0}", decrypt.SequenceEqual(inblock));



        }

        private static void BlowfishBenchmark(byte[] inblock, byte[] key, string keyString)
        {
            var Stopwatch = new Stopwatch();
            Stopwatch.Start();
            Encryption oEncrypt = new Encryption();
            oEncrypt.Padding = PaddingMode.PKCS7;
            oEncrypt.CipherMethod = CipherMode.CBC;


            //Blowfish blowfish = new Blowfish(key);

            var crypted = oEncrypt.EncryptBytes(EncryptionProvider.Blowfish, keyString, inblock);
            //var crypted = blowfish.encryptString(Convert.ToBase64String(inblock));
            Stopwatch.Stop();
            Console.WriteLine("Blowfish - Crypt - Encrypting cost in time in ms: {0}", Stopwatch.ElapsedMilliseconds);
            Console.WriteLine("Crypted file size: {0}", crypted.Length.ToString());

            Stopwatch.Reset();
            Stopwatch.Start();
            //var decrypted = blowfish.decryptString(crypted);

            var decrypt = oEncrypt.DecryptBytes(EncryptionProvider.Blowfish, keyString, crypted);
            Stopwatch.Stop();
            Console.WriteLine("Blowfish - Decrypt - Encrypting cost in time in ms: {0}", Stopwatch.ElapsedMilliseconds);
            Console.WriteLine("Integryty check result: {0}", decrypt.SequenceEqual(inblock));


            //Console.WriteLine("String input: {0}", input);
            //Console.WriteLine("Hash output: {0}", cyphertext);



        }
    }
}
